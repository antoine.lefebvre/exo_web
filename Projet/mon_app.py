from flask import Flask, render_template, request

from model.data import *
# from connection import db

app = Flask(__name__)


@app.route('/')
def index():
    return render_template("index.html", title="Accueil")


@app.route('/users')
def show_users():
    users = get_users()
    app.logger.debug(users)
    # db.init_app(app)
    return render_template("users.html", title="Liste des utilisateurs", users=users)


@app.route('/new_user')
def new_user():
    return render_template("newUser.html", title="Ajouter un utilisateurs")


@app.route('/resultForm', methods=['POST', 'GET'])
def resultForm():
    if request.method == 'POST':
        try:
            name    = request.form['name']
            surname = request.form['surname']
            mail    = request.form['mail']

            msg = name + " " + surname + " à bien été ajouté !"
        except:
            msg = "Erreur :-("

        finally:
            return render_template("resultForm.html", msg=msg)
            con.close()

if __name__ == "__main__":
    app.run()