from flask import Flask, url_for, abort, redirect

app = Flask(__name__)


@app.route('/')
@app.route('/accueil')
def hello_world():
    return 'Hello World !'


@app.route('/profil/<string:username>/<int:age>')
def profil(username, age):
    return "Bonjour " + username + ", vous avez " + str(age) + " ans."


@app.route('/contact')
def contact():
    return "Page de contact <a href='"+url_for("hello_world")+"'>Retour à la page d'accueil</a>"


@app.route('/protected/<int:code>')
def protected(code):
    if code == 1234:
        return "Accès autorisé"
    else:
        return redirect(url_for("user_login"))


@app.route('/login')
def user_login():
    return "Merci de vous identifier"


if __name__ == "__main__":
    app.run()