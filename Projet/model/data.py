def get_users():
    return [
        {
            'first_name': 'toto',
            'last_name': 'endormi',
            'email': 'toto.endormis@gmail.fr'
        },
        {
            'first_name': 'toti',
            'last_name': 'itescia',
            'email': 'toti.itescia@gmail.fr'
        },
        {
            'first_name': 'totu',
            'last_name': 'hs',
            'email': 'totu.hs@gmail.fr'
        }
    ]